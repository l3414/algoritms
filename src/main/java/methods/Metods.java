package methods;
import java.util.Arrays;


//       1) Реализуйте функцию, возвращающую двоичное представление числа n(n>=0).
//       например, 101 - это двоичное представление числа 5. Метод должен иметь сигнатуру String tobinary(int number)
//
//        2) Напишите функцию, меняющую местами значения переменных, не используя временные переменные.
//
//        3) Напишите метод, находящий максимальное из двух чисел, не используя
//        операторы if-else или любые другие операторы сравнения.
//
//        4) Реализуйте метод, определяющий, является ли одна строка перестановкой другой.
//        Под перестановкой понимаем любое изменение порядка символов. Регистр учитывается, пробелы являются существенными
//
//        5) Напишите функцию, которая принимает строку из одного или нескольких слов и
//        возвращает ту же строку, но с перевернутыми всеми словами из пяти или более букв.
//        Передаваемые строки будут состоять только из букв и пробелов. Пробелы будут включены только в том случае, если присутствует более одного слова.
//        Примеры:
//        "This is a test" => "This is a test"
//        "This is another test" => "This is rehtona test"


public class Metods {



    public static String convertToBinary(int digit){

            if(digit==0){
                return "0";
            }else if(digit==1){
                return "1";
            } else if (digit>=2){
                StringBuilder value = new StringBuilder();
                while (digit>0){
                    value.insert(0, digit%2);
                    digit = digit/2;
                }
                return value.toString();

            }else {
            return "Wrong systemArgument";
        }
    }

    public static int[] rotateDigits(int first, int second){
        int[] res = new int[2];
        first = first + second;
        second = first-second;
        first = first-second;
        res[0] = first;
        res[1]= second;
        return res;

    }
    public static int[] rotateDigits(int[] mas){
        return new int[]{mas[1],mas[0]};
        //Просто добавил воды) Это точно не тот метод который ждали, но оно работает :)

    }



    public static int maxValueWithoutcompare(int first, int second){
        return (Math.abs(first+second) + Math.abs(first-second))/2;
    }

    public static Boolean compareString(String first, String second){
        Boolean flag = false;
        char[] firstChar = first.toCharArray();
        char[] secondChar = second.toCharArray();
        Arrays.sort(firstChar);
        Arrays.sort(secondChar);
        if (firstChar.length == secondChar.length){
            for (int i=0; i<first.length(); i++){
                if (firstChar[i] != secondChar[i]){
                    flag = false;
                    break;
                }else flag = true;
            }
        } return flag;
    }

    public static String reversString(String input){
        input.trim();
        String[] arrayString = input.split(" ");
        String res = "";
        for (int i = 0; i<arrayString.length; i++){
            if (arrayString[i].length()>4){
                res += reversWord(arrayString[i])+" ";

            }else {
                res += arrayString[i]+" ";

            }
        }
        return res.trim();

    }

    public static String reversWord(String word){
        char[] wordInput = word.toCharArray();
        String result = "";
        for (int i = wordInput.length - 1; i >= 0; i--) {
            result = result + wordInput[i];
        }
        return result;


    }






}
