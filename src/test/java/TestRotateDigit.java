import methods.Metods;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestRotateDigit {


    @DataProvider
    private Object[][] dataRotate(){
        return new Object[][]{
                {3,5, Metods.rotateDigits(3,5)},
                {12,2, Metods.rotateDigits(12,2)},
                {15,12,  Metods.rotateDigits(15,12)}
        };
    }



    @Test(dataProvider = "dataRotate")
    public void rotateDigitsTest(int first, int second, int[] res){
        Assert.assertEquals(Metods.rotateDigits(first,second),res);

    }
   }


