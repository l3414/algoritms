import methods.Metods;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestMaxWithoutIf {

    @DataProvider
    private Object[][] maxWithoutIf(){
        return new Object[][] {
                {15,10,15},
                {12,6,12},
                {7,75,75},
                {6,15,15}
        };
    }

    @Test(dataProvider = "maxWithoutIf")
    public void maxWithoutIfTest(int first, int second, int res){
        Assert.assertEquals(Metods.maxValueWithoutcompare(first,second), res);
    }

}
