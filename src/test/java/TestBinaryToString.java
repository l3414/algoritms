import methods.Metods;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestBinaryToString {

    @DataProvider
    private Object[][] testBinaryToStringData(){
        return new Object[][] {
                {5, "101"},
                {101, "1100101"},
                {64, "1000000"},
        };
    }

    @Test(dataProvider = "testBinaryToStringData")
    public void testBinaryToString(int input, String res){
        Assert.assertEquals(Metods.convertToBinary(input),res);
    }

}
