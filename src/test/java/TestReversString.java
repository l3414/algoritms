import methods.Metods;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestReversString {

    @DataProvider
    private Object[][] reversStringData(){
        return new Object[][]{
                {"This is another test","This is rehtona test"},
                {"Cold winter, many snow","Cold ,retniw many snow"},
                {"Todo todo todo, psss","Todo todo ,odot psss"},
        };

    }

    @Test(dataProvider = "reversStringData")
    public void reversStringTest(String input, String output){
        Assert.assertEquals(Metods.reversString(input), output);
    }


}
