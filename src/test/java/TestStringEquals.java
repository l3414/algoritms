import methods.Metods;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestStringEquals {
    @DataProvider
    private Object[][] StringEqualsData(){
        return new Object[][]{
                {"Test is test", "Test is test", },
                {"Test is test", "Testis  test"},
                {"Город дорог", "рогдоГо род"},
        };
    }

    @Test(dataProvider = "StringEqualsData")
    public void testStringEquals(String inputFirst, String inputSecond){
        Assert.assertTrue(Metods.compareString(inputFirst,inputSecond));
    }
}
